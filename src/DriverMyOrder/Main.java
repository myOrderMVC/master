package DriverMyOrder;

import java.awt.EventQueue;

import ControllerMyOrder.Controller;
import ModelMyOrder.LoginPageModel;
import ViewMyOrder.LoginPageView;


public class Main
{
	public static void main(String[] args)
	{ 
		LoginPageView LoginView = LoginPageView.CreateInstanceLogin();
		LoginPageModel LoginModel = new LoginPageModel();
		Controller LoginController = new Controller(LoginModel, LoginView);
	}

}
