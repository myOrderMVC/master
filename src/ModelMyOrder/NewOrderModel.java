package ModelMyOrder;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;

import ViewMyOrder.NewOrderView;


public class NewOrderModel {
	
	DefaultTableModel DM;
	private String OrderTitle;
	private SQLlite L;
	private int OrderID=4362;//on func remember to add +1 
	private String Destination;
	private String Bjustification;
	private String Comments;
	private static String DateCreated;//check what func return
	private String VAT="17%";
	private String statusTest;
	private double TotalCost;
	private JTable table;
	private JComboBox<String> CategoryCBox;
	private JComboBox<String> CompanyCBox;
	private JComboBox<String> Sub_CategoryCBox;
	private JSpinner qtyJSpinner;
	
	public JComboBox<String> getCategoryCBox() {
		FillComboCategory();
		return CategoryCBox;
	}
	public void setCategoryCBox(JComboBox<String> categoryCBox) {
		CategoryCBox = categoryCBox;
	}
	public JComboBox<String> getCompanyCBox() {
		BeforeFillCompany();
		return CompanyCBox;
	}
	public void setCompanyCBox(JComboBox<String> companyCBox) {
		CompanyCBox = companyCBox;
	}
	public JComboBox<String> getSub_CategoryCBox() {
		BeforeFillSub_Category();
		return Sub_CategoryCBox;
	}
	public void setSub_CategoryCBox(JComboBox<String> sub_CategoryCBox) {
		Sub_CategoryCBox = sub_CategoryCBox;
	}
	public JSpinner getQtyJSpinner() {
		return qtyJSpinner;
	}
	public void setQtyJSpinner(JSpinner qtyJSpinner) {
		this.qtyJSpinner = qtyJSpinner;
	}
	public JTable getTable() {
		CreateCol();
		return table;
	}
	public void setTable(JTable table) {
		this.table = table;
	}
	//New setters & Getters for usage in page3 summary.
	public String getOrderTitle() {
		return OrderTitle;
	}
	public void setOrderTitle(String orderTitle) {
		this.OrderTitle = orderTitle;
	}
	public int getOrderID() {
		return OrderID;
	}
	public void setOrderID(int orderID) {
		this.OrderID = orderID;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		this.Destination = destination;
	}
	public String getBjustification() {
		return Bjustification;
	}
	public void setBjustification(String bjustification) {
		this.Bjustification = bjustification;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		this.Comments = comments;
	}
	public String DateAndTime()
	{
		LocalDate CurrentDate = LocalDate.now();
		NewOrderModel.DateCreated = CurrentDate.toString();
		return DateCreated;
	}
	private void CreateCol()
	{
		DM=(DefaultTableModel) table.getModel();
		
		DM.addColumn("Category");
		DM.addColumn("Company");
		DM.addColumn("Sub-Category");
		DM.addColumn("Quantity");
	}
	private void AddRowData(String Category,String Company,String Sub_Category,String Quantity)
	{
		String RowData [] = {/*Integer.toString(caunt++),*/Category,Company,Sub_Category,Quantity};
		DM.addRow(RowData);
	}
	private void BeforeFillCompany()
	{
//		Sub_CategoryCBox.removeAllItems();
//		Sub_CategoryCBox.addItem("Select Sub-Category...");
		if(CompanyCBox.getSelectedItem() != null && CompanyCBox.getSelectedItem().toString()!="Select Company...")
			FillComboSub_Category();
	}
	private void BeforeFillSub_Category()
	{
		//QtyCBox.removeAllItems();
		//qtyJSpinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		//qtyJSpinner.setEnabled(false);
		//QtyCBox.addItem("Select Quantity...");
		if(Sub_CategoryCBox.getSelectedItem() != null && Sub_CategoryCBox.getSelectedItem().toString()!="Select Sub-Category...")
		{
			//FillComboQuantity();
			qtyJSpinner.setEnabled(true);
		}
	}
	private void FillComboCategory()
	 {
			 L = SQLlite.CreateInstanceSQL();
			 String Sql ="SELECT DISTINCT Category from Item";
			 ArrayList<String> ListCategory = L.SelectWithQueryList(Sql,"Category");
			 for(int i=0; i<ListCategory.size();i++)
			 {
          	  CategoryCBox.addItem(ListCategory.get(i));
			 }
			// return true;
	 }
	private void FillComboCompany()
	 {
			L = SQLlite.CreateInstanceSQL();
			 String Sql ="SELECT DISTINCT Company from Item where Category=" +"'" +CategoryCBox.getSelectedItem().toString()+"'";
			 ArrayList<String> ListCompany = L.SelectWithQueryList(Sql,"Company");
			 for(int i=0; i<ListCompany.size();i++)
			 {
				 CompanyCBox.addItem(ListCompany.get(i));
			 }
	 }
	private void FillComboSub_Category()
	 {
			 L = SQLlite.CreateInstanceSQL();
			 String Sql ="SELECT DISTINCT Sub_category from Item where Category=" +"'" +CategoryCBox.getSelectedItem().toString()+"'" +"AND Company="+"'"+CompanyCBox.getSelectedItem().toString()+"'";
			 ArrayList<String> ListSub_category = L.SelectWithQueryList(Sql,"Sub_category");
			 for(int i=0; i<ListSub_category.size();i++)
			 {
				 Sub_CategoryCBox.addItem(ListSub_category.get(i));
			 }
	 }
	public void ForCategoryModel()
	{
		FillComboCompany();	
	}

	
}
