package ModelMyOrder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ViewMyOrder.LoginPageView;

public class SQLlite {
	private static SQLlite singelSQLInstance = null;
	private SQLlite() {
		
	}
	
	public static SQLlite CreateInstanceSQL()
	{
		if(singelSQLInstance == null)
		{
			singelSQLInstance = new SQLlite();
		}
		return singelSQLInstance;
	}
	
	public Connection ConnectToDB () 
	{
		String url = "jdbc:sqlite:C:\\Users\\dnisanov\\Desktop\\DB\\myorder.db";
		Connection conn = null;
		try {
				conn = DriverManager.getConnection(url);
				//System.out.println("connection to SQLlite has been established");
			}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
		}
		return conn;
	}
	public void InsertNewOrderPage1(String Query,String OrderTitle, String Destination,String BJustification, String Comments,int OrderID)
	{
		/* example for query 
        String sql = "INSERT INTO NewOrderPage1(OrderTitle,Destination,BJustification,Comments,OrderID) VALUES(?,?,?,?,?)";
        */
		String sql = Query;
        try (Connection conn = this.ConnectToDB();
                PreparedStatement pstmt = conn.prepareStatement(sql))
        {
            pstmt.setString(1, OrderTitle);
            pstmt.setString(2, Destination);
            pstmt.setString(3, BJustification);
            pstmt.setString(4, Comments);
            pstmt.setInt(5, OrderID);
            pstmt.executeUpdate();
        } 
        catch (SQLException e) 
        {
            System.out.println(e.getMessage());
        }
    }
	
	public void SelectAll(String Table)
	{
        String sql = "SELECT * FROM Users";
        
        try (Connection conn = this.ConnectToDB();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql))
        {
            
            // loop through the result set
            while (rs.next()) 
            {
                System.out.println(rs.getInt("ID") +  "\t" + 
                                   rs.getString("name") + "\t" +
                                   rs.getString("username") + "\t" +
                                   rs.getInt("password"));
            }
        } 
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
	public void Select()
	{
        String sql = "SELECT * FROM Users";
        
        try (Connection conn = this.ConnectToDB();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql))
        {
            
            // loop through the result set
            while (rs.next()) 
            {
                System.out.println(rs.getInt("ID") +  "\t" + 
                                   rs.getString("name") + "\t" +
                                   rs.getString("username") + "\t" +
                                   rs.getInt("password"));
            }
        } 
        catch (SQLException e)
        {
            System.out.println(e.getMessage());
        }
    }
	public String SelectWithQuery(String Query,String Var)
	{
        String sql = Query;
        
        try (Connection conn = this.ConnectToDB();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql))
        {
            
            // loop through the result set
        	/*
            while (rs.next()) 
            {
                System.out.println(rs.getInt("ID") +  "\t" + 
                                   rs.getString("name") + "\t" +
                                   rs.getString("username") + "\t" +
                                   rs.getInt("password"));
            }*/
        	
        	return rs.getString(Var);
        } 
        catch (SQLException e)
        {
           // System.out.println(e.getMessage());
        }
        return "null";
    }
	/*
	public ResultSet SelectWithQuery(String Query)
	{
        String sql = Query;
        
        try (Connection conn = this.ConnectToDB();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql))
        {
            
        } 
        catch (SQLException e)
        {
           // System.out.println(e.getMessage());
        }
        return null;
    }
    */
	public ArrayList<String> SelectWithQueryList(String Query,String Var)
	{
        String sql = Query;
        ArrayList<String> List = new ArrayList<String>();
        
        try (Connection conn = this.ConnectToDB();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql))
        {
        	while(rs.next())
            {
           	  List.add(rs.getString(Var));
            }
            return List;
        } 
        catch (SQLException e)
        {
           // System.out.println(e.getMessage());
        }
        return null;
	}

	public static void main(String[] args) throws SQLException {
		String Query = "INSERT INTO NewOrderPage1(OrderTitle,Destination,BJustification,Comments,OrderID) VALUES(?,?,?,?,?)";
		SQLlite l = new SQLlite();
		l.InsertNewOrderPage1(Query, "testTitle", "testDest", "testBJ", "testComm", 1);
		System.out.println("Done");
	}

}
