package ControllerMyOrder;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.ParseException;

import ModelMyOrder.LoginPageModel;
import ModelMyOrder.NewOrderModel;
import ModelMyOrder.forgotPasswordModel;
import ModelMyOrder.itemsModel;
import ViewMyOrder.HomeGuiView;
import ViewMyOrder.LoginPageView;
import ViewMyOrder.NewOrderView;
import ViewMyOrder.NewOrderView2;
import ViewMyOrder.NewOrderView3;
import ViewMyOrder.forgotPasswordPageView;
import ViewMyOrder.itemsView;

public class Controller{
	private LoginPageModel LoginModel;
	private LoginPageView LoginView;
	private Clickedhandler actionHandler;
	private handlerForgetPassword Forgothandler;
	private forgotPasswordPageView forgotPasswordView = null;
	private forgotPasswordModel forgotPassModel = null;
	private itemsView ItemView=null;
	private itemsModel ItemModel=null;
	private HomeGuiView HomeView=null;
	private NewOrderView newOrderView=null;
	private NewOrderModel newOrderModel=null;
	private NewOrderView3 newOrderView3=null;
	private NewOrderView2 newOrderView2=null;
//constructor 	
	public Controller(LoginPageModel LoginModel,LoginPageView LoginView)
	{
		this.LoginModel = LoginModel;
		this.LoginView = LoginView;
		actionHandler = new Clickedhandler();
		Forgothandler = new handlerForgetPassword();
		LoginView.getLoginBtn().addActionListener(actionHandler);
		LoginView.getForgotPasswordLbl().addMouseListener(Forgothandler);
	}
	
	
//Login Page Department	
	private void IfValidate()
	{
		String Uname=LoginView.getUsernameTf().getText();
		LoginModel.SetUserNameModel(Uname);
		String Pass=LoginView.getPasswordTf().getText();
			try {
				if(LoginModel.Validetion(Uname, Pass))
				{
					LoginView.AnswerFromControllerLoginPage(true);
					OpenHomeGui();
				}
				else
				{
					LoginView.AnswerFromControllerLoginPage(false);
				}
				} catch (HeadlessException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
	}
// Home GUI department
	private void OpenHomeGui()
	{
		HomeView = HomeGuiView.CreateInstanceHomeView();
		//HomeView.setVisible(true);
		HomeView.getHomeLogoutBtn().addActionListener(actionHandler);
		HomeView.getHomeItemBtn().addActionListener(actionHandler);
		HomeView.getHomeBillsBtn().addActionListener(actionHandler);
		HomeView.getHomenorderbtn().addActionListener(actionHandler);
		HomeView.getHomereportsbtn().addActionListener(actionHandler);
		SendUserToHomeGui();
	}

	private void SendUserToHomeGui()
	{
		HomeView.SetUserFromController(LoginModel.GetUserNameModel());
		HomeView.LogedInUser();
	}
	private void OpenItemsTab()
	{
		ItemView = new itemsView();
		ItemView.setVisible(true);
		ItemModel = new itemsModel();
		ItemView.getGoBackToHomeGui().addActionListener(actionHandler);
	}
	
//forgotPassword department
	private void getPassword() throws IOException, ParseException
	{
		forgotPassModel.setUserName(forgotPasswordView.getForgottxt().getText());
		forgotPassModel.setID(forgotPasswordView.getForgottxt1().getText());
		String Pass = forgotPassModel.ForgetPass();
		if(Pass == null)
		{
			forgotPasswordView.AnswerFromControllerForgotPasswordPage(false,Pass);
		}
		else
			forgotPasswordView.AnswerFromControllerForgotPasswordPage(true,Pass);
	}
	private void OpenPage1()
	{
		newOrderView = new NewOrderView();
		newOrderView.setVisible(true);
		newOrderModel = new NewOrderModel();
		newOrderView.getNextbtn().addActionListener(actionHandler);
	}
	
	private void OpenPage2()
	{
		newOrderView2 = new NewOrderView2();
		newOrderView2.setVisible(true);
		newOrderView2.getCategoryCBox().addActionListener(actionHandler);
		newOrderView2.getCompanyCBox().addActionListener(actionHandler);
		newOrderView2.getSub_CategoryCBox().addActionListener(actionHandler);
	}
	
	private void OpenPage3() {
		newOrderView3 = new NewOrderView3();
		newOrderView3.getBtnBackPage2().addActionListener(actionHandler);
		newOrderView3.setVisible(true);
		getOrderDataToView();
		//newOrderView.New
	}
	private void GetAndSetTableView2()
	{
		newOrderModel.setTable(newOrderView2.getTable());
		//newOrderModel.CreateCol();
		newOrderView2.setTable(newOrderModel.getTable());
	}
	private void GetAndSetCategoryCBOXView2()
	{
		newOrderModel.setCategoryCBox(newOrderView2.getCategoryCBox());
		//newOrderModel.setQtyJSpinner(newOrderView2.getQtyJSpinner());
		newOrderView2.setCategoryCBox(newOrderModel.getCategoryCBox());
		//newOrderView2.setQtyJSpinner(newOrderModel.getQtyJSpinner());
	}
	private void GetAndSetCompanyCBOXView2()
	{
		newOrderModel.setCompanyCBox(newOrderView2.getCompanyCBox());
		newOrderView2.ForCategory1View2();
		newOrderModel.ForCategoryModel();
		newOrderView2.ForCategory2View2();
		newOrderView2.setCompanyCBox(newOrderModel.getCompanyCBox());
	}
	private void GetAndSetSub_CategoryCBOXView2()
	{
		newOrderModel.setSub_CategoryCBox(newOrderView2.getSub_CategoryCBox());
		newOrderView2.setSub_CategoryCBox(newOrderModel.getSub_CategoryCBox());
	}
	private void setOrderDetailsToModel() {
		newOrderModel.setOrderTitle(newOrderView.getOrderTitleTxt().getText());
		newOrderModel.setDestination(newOrderView.getDestinationTxt().getSelectedItem().toString());
		newOrderModel.setBjustification(newOrderView.getBjustificationTxt().getText());
		newOrderModel.setComments(newOrderView.getCommentsTxt().getText());
	}
	private void getOrderDataToView() {
		newOrderView3.setTxtOrderID_Data(String.valueOf(Integer.valueOf(newOrderModel.getOrderID())));
		newOrderView3.setLblOrderTitle(newOrderModel.getOrderTitle());
		newOrderView3.setTxtDestinationData(newOrderModel.getDestination());
		newOrderView3.setTxtBJustData(newOrderModel.getBjustification());
		newOrderView3.setTxtCommentsData(newOrderModel.getComments());
		newOrderView3.setTxtCreatedDateData(newOrderModel.DateAndTime());
	}
	
//Items department
	public void getItems()
	{
		ItemModel.getItems();
	}
	
	
//Action Listener \ Mouse listener department
	private class handlerForgetPassword implements MouseListener
	{
		public void mouseClicked(MouseEvent arg0) {
			forgotPasswordView = new forgotPasswordPageView();
			forgotPasswordView.setVisible(true);
			forgotPassModel = new forgotPasswordModel();
			forgotPasswordView.getBtnSubmit().addActionListener(actionHandler);
			forgotPasswordView.getExitBtn().addActionListener(actionHandler);
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	private class Clickedhandler implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == LoginView.getLoginBtn())
			{
				IfValidate();
			}
			else if(forgotPasswordView != null && e.getSource() == forgotPasswordView.getExitBtn())
			{
				forgotPasswordView.ifExit();
			}
			else if(forgotPasswordView != null && e.getSource() == forgotPasswordView.getBtnSubmit())
			{
				try {
					getPassword();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}
			else if(HomeView !=null  && e.getSource() == HomeView.getHomeItemBtn())
			{
				OpenItemsTab();
				HomeView.HomeViewDispose();
			}
			else if(HomeView != null && e.getSource() == HomeView.getHomeLogoutBtn())
			{
				LoginPageView.CreateInstanceLogin();
				HomeView.HomeViewDispose();
				LoginView.setVisible(true);
				LoginView.ClearUserAndPassword();
			}
			else if(HomeView != null && e.getSource() == HomeView.getHomenorderbtn())
			{
				OpenPage1();
				HomeView.HomeViewDispose();
			}
			else if(HomeView != null && e.getSource() == HomeView.getHomeBillsBtn())
			{
				HomeView.billsLog();
			}
			else if(ItemView != null && e.getSource() == ItemView.getGoBackToHomeGui())
			{
				//OpenHomeGui();
				HomeView.CreateInstanceHomeView();
				HomeView.setVisible(true);
				ItemView.itemViewDispose();
			}
			else if(newOrderView != null && e.getSource() == newOrderView.getNextbtn())
			{
				setOrderDetailsToModel();
				OpenPage2();
				newOrderView.NewOrderViewDispose();
				GetAndSetTableView2();
				GetAndSetCategoryCBOXView2();
				
			}
			else if(newOrderView2 != null && e.getSource() == newOrderView2.getNextbtn())
			{
				setOrderDetailsToModel();
				OpenPage3();
				newOrderView2.NewOrderView2Dispose();
			}
			else if(newOrderView2 != null && e.getSource() == newOrderView2.getCategoryCBox())
			{
				GetAndSetCompanyCBOXView2();
			}
			else if(newOrderView2 != null && e.getSource() == newOrderView2.getCompanyCBox())
			{
				GetAndSetSub_CategoryCBOXView2();
			}
//			else if(newOrderView2 != null && e.getSource() == newOrderView2.getSub_CategoryCBox())
//			{
//				GetAndSetSub_CategoryCBOXView2();
//			}
			else if(newOrderView3 != null && e.getSource() == newOrderView3.getBtnBackPage2()) {
				newOrderView3.setVisible(false);
				newOrderView.setVisible(true);
			}
		}
	}
}
