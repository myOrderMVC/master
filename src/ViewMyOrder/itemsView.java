package ViewMyOrder;
import java.awt.Color;
import java.awt.Font;
import java.sql.ResultSet;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class itemsView extends JFrame{
	private JPanel contentPane;
	private JTable table;
	private JLabel lblI;
	private JButton goBackToHomeGui;
	
	public itemsView() {
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setForeground(Color.BLACK);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1621, 876);
		getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(27, 238, 1517, 205);
		getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		lblI = new JLabel("Items");
     	lblI.setFont(new Font("Tahoma", Font.PLAIN, 26));
     	lblI.setBounds(27, 78, 225, 42);
     	getContentPane().add(lblI);
     	
     	goBackToHomeGui = new JButton("Back");
     	goBackToHomeGui.setBounds(1378, 756, 132, 42);
     	getContentPane().add(goBackToHomeGui);
     	 
	}
	public JButton getGoBackToHomeGui() {
		return goBackToHomeGui;
	}
	public void setGoBackToHomeGui(JButton goBackToHomeGui) {
		this.goBackToHomeGui = goBackToHomeGui;
	}
	public void itemViewDispose() {
		dispose();
	}
	public void resultFromController(Boolean answer,ResultSet rs)
	{
		if(answer)
			;//table.setModel(DBUtils.resultSetToTableModel(rs));
		else
			System.out.println("The return value of the Itmes is not good");
	}
}

