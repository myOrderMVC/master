package ViewMyOrder;

import java.awt.Color;
import java.awt.Font;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;

public class NewOrderView2 extends JFrame {
	
	private String Category;
	private String Company;
	private JComboBox<String> CategoryCBox;
	private JComboBox<String> CompanyCBox;
	private JComboBox<String> Sub_CategoryCBox;
	private JComboBox QtyCBox;
	private JSpinner qtyJSpinner;
	private  JTable table;
	private JButton Nextbtn;
	
		public JButton getNextbtn() {
		return Nextbtn;
	}

	public void setNextbtn(JButton nextbtn) {
		Nextbtn = nextbtn;
	}

		public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
	public void NewOrderView2Dispose()
	{
		dispose();
	}

		public JComboBox<String> getCategoryCBox() {
		return CategoryCBox;
	}

	public void setCategoryCBox(JComboBox<String> categoryCBox) {
		CategoryCBox = categoryCBox;
	}

	public JComboBox<String> getCompanyCBox() {
		return CompanyCBox;
	}

	public void setCompanyCBox(JComboBox<String> companyCBox) {
		CompanyCBox = companyCBox;
	}

	public JComboBox<String> getSub_CategoryCBox() {
		return Sub_CategoryCBox;
	}

	public void setSub_CategoryCBox(JComboBox<String> sub_CategoryCBox) {
		Sub_CategoryCBox = sub_CategoryCBox;
	}

	public JSpinner getQtyJSpinner() {
		return qtyJSpinner;
	}

	public void setQtyJSpinner(JSpinner qtyJSpinner) {
		this.qtyJSpinner = qtyJSpinner;
	}

		public NewOrderView2() 
		{
			
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setForeground(Color.BLACK);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1621, 876);
		getContentPane().setLayout(null);
		
		CategoryCBox = new JComboBox<String>();
		CategoryCBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Select Category..."}));
		CategoryCBox.setBounds(81, 66, 137, 28);
		getContentPane().add(CategoryCBox);
		
		CompanyCBox = new JComboBox();
		CompanyCBox.setModel(new DefaultComboBoxModel(new String[] {"Select Company..."}));
		CompanyCBox.setBounds(234, 66, 137, 28);
		getContentPane().add(CompanyCBox);
		
		Sub_CategoryCBox = new JComboBox();
		Sub_CategoryCBox.setModel(new DefaultComboBoxModel(new String[] {"Select Sub-Category..."}));
		Sub_CategoryCBox.setBounds(388, 66, 137, 28);
		getContentPane().add(Sub_CategoryCBox);
		
		JButton AddBtn = new JButton("Add");
		AddBtn.setBounds(790, 69, 89, 23);
		getContentPane().add(AddBtn);
		
		qtyJSpinner = new JSpinner();
		qtyJSpinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		qtyJSpinner.setEnabled(false);
		qtyJSpinner.setBounds(606, 66, 52, 28);
		getContentPane().add(qtyJSpinner);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(32, 369, 1520, 338);
		getContentPane().add(scrollPane);
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.BOLD, 12));
		table.setBackground(Color.WHITE);
		table.setBounds(37, 247, 1518, 579);
		scrollPane.setViewportView(table);
		
		Nextbtn = new JButton("Next");
		Nextbtn.setBounds(72, 803, 89, 23);
		getContentPane().add(Nextbtn);
		
		JButton btnRemoveSelectedRow = new JButton("Remove selected ROW");
		btnRemoveSelectedRow.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnRemoveSelectedRow.setBounds(32, 309, 231, 28);
		getContentPane().add(btnRemoveSelectedRow);
		
		
		
		}
		public void ForCategory1View2()
		{
			CompanyCBox.removeAllItems();
			CompanyCBox.addItem("Select Company...");
			if(CompanyCBox.getSelectedItem().toString()=="Select Company..." && CategoryCBox.getSelectedItem().toString() == "Select Category..." )
				CompanyCBox.removeAllItems();
		}
		public void ForCategory2View2()
		{
			if(CategoryCBox.getSelectedItem().toString() == "Select Category...")
			{
				CompanyCBox.addItem("Select Company...");
			}
		}

}
