package ViewMyOrder;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ModelMyOrder.SQLlite;

public class forgotPasswordPageView extends JFrame {
	private JPanel contentPane;
	private JTextField forgottxt;
	private JButton btnSubmit;
	private JButton ExitBtn;
	private int count=0;
	private JTextField forgottxt1;
	
	public JPanel getContentPane() {
		return contentPane;
	}
	public JTextField getForgottxt() {
		return forgottxt;
	}
	public void setForgottxt(JTextField forgottxt) {
		this.forgottxt = forgottxt;
	}
	public JButton getBtnSubmit() {
		return btnSubmit;
	}
	public void setBtnSubmit(JButton btnSubmit) {
		this.btnSubmit = btnSubmit;
	}
	public JButton getExitBtn() {
		return this.ExitBtn;
	}
	public void setExitBtn(JButton exitBtn) {
		this.ExitBtn = exitBtn;
	}
	public JTextField getForgottxt1() {
		return forgottxt1;
	}
	public void setForgottxt1(JTextField forgottxt1) {
		this.forgottxt1 = forgottxt1;
	}
	public forgotPasswordPageView() {
		System.out.println("forgot password constractor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel forgotlbl1 = new JLabel("Please enter your username: ");
		forgotlbl1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		forgotlbl1.setBounds(10, 44, 217, 32);
		contentPane.add(forgotlbl1);
		
		forgottxt = new JTextField();
		forgottxt.setBounds(226, 46, 174, 32);
		contentPane.add(forgottxt);
		forgottxt.setColumns(10);
		
		btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(170, 154, 89, 23);
		contentPane.add(btnSubmit);
		
		ExitBtn = new JButton("Exit");
		ExitBtn.setBounds(170, 212, 89, 23);
		contentPane.add(ExitBtn);

		JLabel forgotlbl2 = new JLabel("Please enter your ID:");
		forgotlbl2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		forgotlbl2.setBounds(10, 88, 206, 32);
		contentPane.add(forgotlbl2);
		
		forgottxt1 = new JTextField();
		forgottxt1.setColumns(10);
		forgottxt1.setBounds(226, 87, 174, 32);
		contentPane.add(forgottxt1);
		
		JLabel lblPleaseEnterYour = new JLabel("Please enter your username and ID to recover password");
		lblPleaseEnterYour.setForeground(Color.RED);
		lblPleaseEnterYour.setBackground(Color.BLACK);
		lblPleaseEnterYour.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPleaseEnterYour.setBounds(10, 11, 414, 32);
		contentPane.add(lblPleaseEnterYour);
		contentPane.getRootPane().setDefaultButton(btnSubmit);
	}
	public void ifExit()
	{
		dispose();
	}
	
	public void AnswerFromControllerForgotPasswordPage(Boolean answer,String password)
	{
		if(answer)
		{
			JOptionPane.showMessageDialog(null,password,"Your Password",JOptionPane.WARNING_MESSAGE);
			System.out.println("the password is: fuck you!!!");
			dispose();
		}
		else
		{
			if(count == 2)
				dispose();
			count+=1;
			JOptionPane.showMessageDialog(null,"User name OR Password invalid","Warning",JOptionPane.PLAIN_MESSAGE);
		}
		
	}

}

