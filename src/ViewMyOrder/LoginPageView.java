package ViewMyOrder;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ControllerMyOrder.Controller;


import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class LoginPageView extends JFrame {

	private JPanel contentPane;
	private static LoginPageView SingleLoginPage = null;
	private JTextField UsernameTf;
	private static  String UserName;
	private String Password;
	private JPasswordField PasswordTf;
	private JLabel ForgotPasswordLbl;
	private JButton LoginBtn;
	private JLabel PasswordLbl;
	private JLabel UsernameLbl;
	private JLabel myOrderLbl;
	private JFrame LoginFrame;
	
	public JTextField getUsernameTf() {
		return UsernameTf;
	}
	public void setUsernameTf(JTextField usernameTf) {
		this.UsernameTf = usernameTf;
	}
	public JPasswordField getPasswordTf() {
		return PasswordTf;
	}
	public void setPasswordTf(JPasswordField passwordTf) {
		this.PasswordTf = passwordTf;
	}
	public JLabel getForgotPasswordLbl() {
		return ForgotPasswordLbl;
	}
	public void setForgotPasswordLbl(JLabel forgotPasswordLbl) {
		this.ForgotPasswordLbl = forgotPasswordLbl;
	}
	public JButton getLoginBtn() {
		return LoginBtn;
	}
	public void setLoginBtn(JButton loginBtn) {
		this.LoginBtn = loginBtn;
	}

	public void ClearPasswordTF() 
	{
		PasswordTf.setText(null);
	}
	public void ClearUserTF()
	{
		UsernameTf.setText(null);
	}
	public static String GetUserName()
	{
		return UserName;
	}
	public void SetUserName(String UserName)
	{
		LoginPageView.UserName = UserName;
	}
	public void SetPassword(String Password)
	{
		this.Password = Password;
	}

	private LoginPageView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		myOrderLbl = new JLabel("myOrder");
		myOrderLbl.setBounds(98, 26, 115, 25);
		contentPane.add(myOrderLbl);
		
		UsernameLbl = new JLabel("UserName:");
		UsernameLbl.setBounds(10, 86, 107, 25);
		contentPane.add(UsernameLbl);
		
		PasswordLbl = new JLabel("Password:");
		PasswordLbl.setBounds(10, 122, 107, 25);
		contentPane.add(PasswordLbl);
		
		UsernameTf = new JTextField();
		UsernameTf.setBounds(76, 88, 168, 25);
		contentPane.add(UsernameTf);
		UsernameTf.setColumns(10);
		
		LoginBtn = new JButton("Login");
		LoginBtn.setBounds(40, 197, 124, 33);
		contentPane.add(LoginBtn);
		
		ForgotPasswordLbl = new JLabel("Forgot Password");
		ForgotPasswordLbl.setBounds(106, 158, 107, 14);
		contentPane.add(ForgotPasswordLbl);
		
		PasswordTf = new JPasswordField();
		PasswordTf.setBounds(76, 122, 168, 25);
		contentPane.add(PasswordTf);
		contentPane.getRootPane().setDefaultButton(LoginBtn);
	}
	public static LoginPageView CreateInstanceLogin()
	{
		if(SingleLoginPage == null)
		{
			SingleLoginPage = new LoginPageView();
			SingleLoginPage.setVisible(true);
		}
		return SingleLoginPage;
	}
	public void AnswerFromControllerLoginPage(Boolean answer)
	{
		if(answer)
		{
			dispose();
		}
		else
		{
			ClearPasswordTF();
			JOptionPane.showMessageDialog(null,"User name OR Password invalid","Warning",JOptionPane.PLAIN_MESSAGE);
		}
		
	}
	public void ClearUserAndPassword() {
		ClearUserTF();
		ClearPasswordTF();	
	}

}
