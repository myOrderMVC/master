package ViewMyOrder;

import java.awt.Color;
import java.awt.Font;
import java.time.LocalDate;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class NewOrderView extends JFrame{
		
		private JButton Nextbtn;//Updated @Albert
		
		private JPanel contentPane;
		private JTextField txtOrderID_Data;
		private JTextField OrderTitleTxt;
		private JComboBox DestanetionCBox;
		private JTextArea BjustificationTxt;
		private JTextArea CommentsTxt;
		
		
		public JTextField getTxtOrderID_Data() {
			return txtOrderID_Data;
		}

		public void setTxtOrderID_Data(String txtOrderID_Data) {
			this.txtOrderID_Data.setText(txtOrderID_Data);
		}
		
		public JComboBox getDestinationTxt() {
			return DestanetionCBox;
		}
		public void setDestinationTxt(JComboBox destinationTxt) {
			DestanetionCBox = destinationTxt;
		}
		public JTextArea getBjustificationTxt() {
			return BjustificationTxt;
		}
		public void setBjustificationTxt(JTextArea bjustificationTxt) {
			BjustificationTxt = bjustificationTxt;
		}
		public JTextArea getCommentsTxt() {
			return CommentsTxt;
		}
		public void setCommentsTxt(JTextArea commentsTxt) {
			CommentsTxt = commentsTxt;
		}
		public JTextField getOrderTitleTxt() {
			return OrderTitleTxt;
		}
		public void setOrderTitleTxt(JTextField orderTitleTxt) {
			OrderTitleTxt = orderTitleTxt;
		}
		
		//Setter & Getter nextBtn @Albert
		public JButton getNextbtn() {
			return Nextbtn;
		}
		public void setNextbtn(JButton nextbtn) {
			Nextbtn = nextbtn;
		}
		public void NewOrderViewDispose()
		{
			dispose();
		}
		//Copied from NewOrderPage@Albert
		public NewOrderView() {
			
			getContentPane().setBackground(Color.WHITE);
			getContentPane().setForeground(Color.BLACK);
			setExtendedState(JFrame.MAXIMIZED_BOTH); 
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 1621, 876);
			getContentPane().setLayout(null);
			
			JLabel OrderTitleLbl = new JLabel("Order Title:");
			OrderTitleLbl.setFont(new Font("Tahoma", Font.PLAIN, 14));
			OrderTitleLbl.setBounds(50, 118, 82, 24);
			getContentPane().add(OrderTitleLbl);
			
			OrderTitleTxt = new JTextField();
			OrderTitleTxt.setBounds(123, 117, 215, 30);
			getContentPane().add(OrderTitleTxt);
			OrderTitleTxt.setColumns(10);
			
			JLabel OrderIDLbl = new JLabel("Order ID:");
			OrderIDLbl.setFont(new Font("Tahoma", Font.PLAIN, 14));
			OrderIDLbl.setBounds(50, 182, 69, 20);
			getContentPane().add(OrderIDLbl);
			
			txtOrderID_Data = new JTextField();
			txtOrderID_Data.setFont(new Font("Tahoma", Font.BOLD, 20));
			txtOrderID_Data.setHorizontalAlignment(SwingConstants.CENTER);
			txtOrderID_Data.setEditable(false);
			txtOrderID_Data.setEditable(false);
			txtOrderID_Data.setBounds(118, 182, 220, 24);
			getContentPane().add(txtOrderID_Data);
			txtOrderID_Data.setColumns(10);
			
			JLabel DestinetionLbl = new JLabel("Destination:");
			DestinetionLbl.setFont(new Font("Tahoma", Font.PLAIN, 14));
			DestinetionLbl.setBounds(50, 250, 82, 24);
			getContentPane().add(DestinetionLbl);
			
			DestanetionCBox = new JComboBox();
			DestanetionCBox.setModel(new DefaultComboBoxModel(new String[] {"Please select dest...","Haifa", "Ashdod", "Tel-Aviv", "Ashkelon", "Sderot"}));
			DestanetionCBox.setBounds(142, 252, 201, 24);
			getContentPane().add(DestanetionCBox);
			
			JLabel lblBjustification = new JLabel("B.Justification:");
			lblBjustification.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblBjustification.setBounds(50, 319, 96, 24);
			getContentPane().add(lblBjustification);
			
			BjustificationTxt = new JTextArea();
			BjustificationTxt.setBounds(171, 319, 236, 89);
			BjustificationTxt.setBackground(Color.lightGray);
			Border roundedBorder = new LineBorder(Color.black, 1, false);
			BjustificationTxt.setBorder(roundedBorder);
			getContentPane().add(BjustificationTxt);
			
			JLabel Commentslbl = new JLabel("Comments:");
			Commentslbl.setFont(new Font("Tahoma", Font.PLAIN, 14));
			Commentslbl.setBounds(50, 463, 96, 24);
			getContentPane().add(Commentslbl);
			
			CommentsTxt = new JTextArea();
			CommentsTxt.setBounds(171, 463, 236, 89);
			CommentsTxt.setBackground(Color.lightGray);
			Border roundedBorderComments = new LineBorder(Color.black, 1, false);
			CommentsTxt.setBorder(roundedBorderComments);
			getContentPane().add(CommentsTxt);
			
			
			Nextbtn = new JButton("Next");//Updated@Albert

			Nextbtn.setBounds(118, 743, 138, 38);
			getContentPane().add(Nextbtn);
			
			JLabel lblNewOrder = new JLabel("New Order");
			lblNewOrder.setFont(new Font("Tahoma", Font.BOLD, 18));
			lblNewOrder.setBounds(50, 67, 147, 30);
			getContentPane().add(lblNewOrder);
		}
}
