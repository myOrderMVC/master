package ViewMyOrder;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class HomeGuiView extends JFrame {
	
	private JLabel homeuserlbl1;
	private JButton homenorderbtn;
	private JButton homereportsbtn;
	private JButton HomeItemBtn;
	private JButton HomeDraftsBtn;
	private JButton HomeBillsBtn;
	private JLabel HomeFeedbacklbl;
	private JButton HomeLogoutBtn;
	private String UserFromLogin;
	private static HomeGuiView CreateInstanceHomeView=null;

	
	private HomeGuiView()
	{
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setForeground(Color.BLACK);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1621, 876);
		getContentPane().setLayout(null);
		
//		homeuserlbl1 = new JLabel(UserFromLogin);
//		homeuserlbl1.setBounds(1386, 13, 46, 14);
//		getContentPane().add(homeuserlbl1);
		
		homenorderbtn = new JButton("New Order");
		homenorderbtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		homenorderbtn.setBounds(29, 90, 122, 37);
		getContentPane().add(homenorderbtn);
		
		homereportsbtn = new JButton("Reports");
		homereportsbtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		homereportsbtn.setBounds(29, 165, 122, 37);
		getContentPane().add(homereportsbtn);
		
		HomeItemBtn = new JButton("Item");
		HomeItemBtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		HomeItemBtn.setBounds(29, 238, 122, 37);
		getContentPane().add(HomeItemBtn);
		
		HomeDraftsBtn = new JButton("Drafts");
		HomeDraftsBtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		HomeDraftsBtn.setBounds(29, 316, 122, 37);
		getContentPane().add(HomeDraftsBtn);
		
		HomeBillsBtn = new JButton("Bills");
		HomeBillsBtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		HomeBillsBtn.setBounds(29, 395, 122, 37);
		getContentPane().add(HomeBillsBtn);
		
		HomeFeedbacklbl = new JLabel("Give Feedback");
		HomeFeedbacklbl.setToolTipText("");
		HomeFeedbacklbl.setFont(new Font("Tahoma", Font.PLAIN, 16));
		HomeFeedbacklbl.setForeground(Color.RED);
		HomeFeedbacklbl.setBounds(1406, 779, 147, 27);
		getContentPane().add(HomeFeedbacklbl);
		
		HomeLogoutBtn = new JButton("LogOut");
		HomeLogoutBtn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		HomeLogoutBtn.setBounds(1444, 0, 150, 27);
		getContentPane().add(HomeLogoutBtn);
		
		JLabel lblUserLogedIn = new JLabel("User Logged In:");
		lblUserLogedIn.setBounds(1444, 30, 109, 27);
		lblUserLogedIn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		getContentPane().add(lblUserLogedIn);
	}
	
	public static HomeGuiView CreateInstanceHomeView()
	{
		if(CreateInstanceHomeView==null)
		{
			CreateInstanceHomeView = new HomeGuiView();
			CreateInstanceHomeView.setVisible(true);
		}
		return CreateInstanceHomeView;
	}

	
	public JLabel getHomeuserlbl1() {
		return homeuserlbl1;
	}

	public void setHomeuserlbl1(JLabel homeuserlbl1) {
		this.homeuserlbl1 = homeuserlbl1;
	}

	public JButton getHomenorderbtn() {
		return homenorderbtn;
	}

	public void setHomenorderbtn(JButton homenorderbtn) {
		this.homenorderbtn = homenorderbtn;
	}

	public JButton getHomereportsbtn() {
		return homereportsbtn;
	}

	public void setHomereportsbtn(JButton homereportsbtn) {
		this.homereportsbtn = homereportsbtn;
	}

	public JButton getHomeItemBtn() {
		return HomeItemBtn;
	}

	public void setHomeItemBtn(JButton homeItemBtn) {
		HomeItemBtn = homeItemBtn;
	}

	public JButton getHomeDraftsBtn() {
		return HomeDraftsBtn;
	}

	public void setHomeDraftsBtn(JButton homeDraftsBtn) {
		HomeDraftsBtn = homeDraftsBtn;
	}

	public JButton getHomeBillsBtn() {
		return HomeBillsBtn;
	}

	public void setHomeBillsBtn(JButton homeBillsBtn) {
		HomeBillsBtn = homeBillsBtn;
	}

	public JLabel getHomeFeedbacklbl() {
		return HomeFeedbacklbl;
	}

	public void setHomeFeedbacklbl(JLabel homeFeedbacklbl) {
		HomeFeedbacklbl = homeFeedbacklbl;
	}

	public JButton getHomeLogoutBtn() {
		return HomeLogoutBtn;
	}

	public void setHomeLogoutBtn(JButton homeLogoutBtn) {
		HomeLogoutBtn = homeLogoutBtn;
	}

	public void HomeViewDispose() {
		dispose();
	}
	
	public void LogedInUser()
	{
		homeuserlbl1 = new JLabel(GetUserFromController());
		homeuserlbl1.setBounds(1550, 30, 137, 27);
		homeuserlbl1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		getContentPane().add(homeuserlbl1);
	}

	public void SetUserFromController(String User) 
	{
		this.UserFromLogin = User;
	}
	public String GetUserFromController()
	{
		return this.UserFromLogin;
	}
	
	public void billsLog() 
	{
		JOptionPane.showMessageDialog(null,"Bills option will be implemented in next release","Warning",JOptionPane.PLAIN_MESSAGE);
	}
}
