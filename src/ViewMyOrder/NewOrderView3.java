package ViewMyOrder;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.omg.CORBA.PUBLIC_MEMBER;


public class NewOrderView3 extends JFrame{
	
	private JPanel contentPane;
	private JTextField txtOrderID_Data;
	private JTextField txtDestinationData;
	private JTextField txtCreatedDateData;
	private JTextField txtStatusData;
	private JTextField txtVatdata;
	private JTextField txtBJustData;
	private JTextField txtCommentsData;
	private JTextField txtTotalCostData;
	private JTable tableItemsData;
	
	private JLabel lblOrderTitle;
	
	
	
	public JLabel getLblOrderTitle() {
		return lblOrderTitle;
	}

	public void setLblOrderTitle(String lblOrderTitle) {
		this.lblOrderTitle.setText(lblOrderTitle);
	}

	public JTextField getTxtOrderID_Data() {
		return txtOrderID_Data;
	}

	public void setTxtOrderID_Data(String txtOrderID_Data) {
		this.txtOrderID_Data.setText(txtOrderID_Data);
	}

	public JTextField getTxtDestinationData() {
		return txtDestinationData;
	}

	public void setTxtDestinationData(String txtDestinationData) {
		this.txtDestinationData.setText(txtDestinationData);
	}

	public JTextField getTxtCreatedDateData() {
		return txtCreatedDateData;
	}

	public void setTxtCreatedDateData(String txtCreatedDateData) {
		this.txtCreatedDateData.setText(txtCreatedDateData);
	}

	public JTextField getTxtBJustData() {
		return txtBJustData;
	}

	public void setTxtBJustData(String txtBJustData) {
		this.txtBJustData.setText(txtBJustData);
	}

	public JTextField getTxtCommentsData() {
		return txtCommentsData;
	}

	public void setTxtCommentsData(String txtCommentsData) {
		this.txtCommentsData.setText(txtCommentsData);
	}

	//Updated @Albert
	private JButton btnBackPage2;

	public JButton getBtnBackPage2() {
		return btnBackPage2;
	}

	public void setBtnBackPage2(JButton btnBackPage2) {
		this.btnBackPage2 = btnBackPage2;
	}

	/**
	 * Create the frame.
	 */
	public NewOrderView3() {
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1578, 971);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//Updated NewOrderView.GetOrder @Albert
		lblOrderTitle = new JLabel("");//(NewOrderView.GetOrderTitle());
		lblOrderTitle.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblOrderTitle.setBounds(50, 70, 492, 34);
		contentPane.add(lblOrderTitle);
		
		JLabel lblOrderid = new JLabel("OrderID:");
		lblOrderid.setBounds(50, 120, 135, 20);
		contentPane.add(lblOrderid);
		
		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setBounds(50, 170, 135, 20);
		contentPane.add(lblDestination);
		
		txtOrderID_Data = new JTextField();
		txtOrderID_Data.setFont(new Font("Tahoma", Font.BOLD, 20));
		txtOrderID_Data.setHorizontalAlignment(SwingConstants.CENTER);
		txtOrderID_Data.setEnabled(false);
		txtOrderID_Data.setEditable(false);
		txtOrderID_Data.setBounds(151, 120, 146, 26);
		contentPane.add(txtOrderID_Data);
		txtOrderID_Data.setColumns(10);
		
		
		txtDestinationData = new JTextField();
		txtDestinationData.setFont(new Font("Times New Roman", Font.BOLD, 20));
		txtDestinationData.setHorizontalAlignment(SwingConstants.CENTER);
		txtDestinationData.setEnabled(false);
		txtDestinationData.setEditable(false);
		txtDestinationData.setColumns(10);
		txtDestinationData.setBounds(151, 170, 146, 26);
		contentPane.add(txtDestinationData);
		
		
		JLabel lblCreated = new JLabel("Created:");
		lblCreated.setBounds(50, 220, 69, 20);
		contentPane.add(lblCreated);
		
		txtCreatedDateData = new JTextField();
		txtCreatedDateData.setHorizontalAlignment(SwingConstants.CENTER);
		txtCreatedDateData.setFont(new Font("Tahoma", Font.BOLD, 20));
		txtCreatedDateData.setEnabled(false);
		txtCreatedDateData.setEditable(false);
		txtCreatedDateData.setColumns(10);
		txtCreatedDateData.setBounds(151, 220, 146, 26);
		contentPane.add(txtCreatedDateData);
		
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(50, 270, 69, 20);
		contentPane.add(lblStatus);
		
		txtStatusData = new JTextField();
		txtStatusData.setBackground(Color.ORANGE);//Once order created, 1st status is pending approval(orange).
		txtStatusData.setHorizontalAlignment(SwingConstants.CENTER);
		txtStatusData.setEnabled(false);
		txtStatusData.setEditable(false);
		txtStatusData.setColumns(10);
		txtStatusData.setBounds(151, 270, 146, 26);
		contentPane.add(txtStatusData);
		
		txtStatusData.setText("Pending Approval");
		
		JLabel lblVat = new JLabel("VAT:");
		lblVat.setBounds(50, 320, 69, 20);
		contentPane.add(lblVat);
		
		txtVatdata = new JTextField();
		txtVatdata.setFont(new Font("Tahoma", Font.BOLD, 20));
		txtVatdata.setForeground(Color.BLACK);
		txtVatdata.setHorizontalAlignment(SwingConstants.CENTER);
		txtVatdata.setEditable(false);
		txtVatdata.setEnabled(false);
		txtVatdata.setText("17%");
		txtVatdata.setBounds(151, 320, 146, 26);
		contentPane.add(txtVatdata);
		txtVatdata.setColumns(10);
		
		JLabel lblBjustification = new JLabel("B.Justification:");
		lblBjustification.setBounds(50, 370, 103, 20);
		contentPane.add(lblBjustification);
		
		txtBJustData = new JTextField();
		txtBJustData.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtBJustData.setEnabled(false);
		txtBJustData.setEditable(false);
		txtBJustData.setColumns(10);
		txtBJustData.setBounds(151, 370, 659, 59);
		contentPane.add(txtBJustData);
		
		
		JLabel lblComments = new JLabel("Comments:");
		lblComments.setBounds(50, 440, 103, 20);
		contentPane.add(lblComments);
		
		txtCommentsData = new JTextField();
		txtCommentsData.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtCommentsData.setHorizontalAlignment(SwingConstants.LEFT);
		txtCommentsData.setEnabled(false);
		txtCommentsData.setEditable(false);
		txtCommentsData.setColumns(10);
		txtCommentsData.setBounds(151, 440, 659, 59);
		contentPane.add(txtCommentsData);
		
		
		JButton btnClosePage3 = new JButton("Submit");

		btnClosePage3.setBounds(1389, 851, 115, 29);
		contentPane.add(btnClosePage3);
		
		JLabel lblTotalCost = new JLabel("Total Cost:");
		lblTotalCost.setBounds(836, 120, 135, 20);
		contentPane.add(lblTotalCost);
		
		txtTotalCostData = new JTextField();
		txtTotalCostData.setEnabled(false);
		txtTotalCostData.setEditable(false);
		txtTotalCostData.setColumns(10);
		txtTotalCostData.setBounds(928, 120, 146, 26);
		contentPane.add(txtTotalCostData);
		
		JLabel lblLineItems = new JLabel("Line Items:");
		lblLineItems.setBounds(50, 560, 103, 20);
		contentPane.add(lblLineItems);
		
		tableItemsData = new JTable();
		tableItemsData.setBackground(Color.LIGHT_GRAY);
		tableItemsData.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column", "New column"
			}
		));
		tableItemsData.setColumnSelectionAllowed(true);
		tableItemsData.setCellSelectionEnabled(true);
		tableItemsData.setBounds(152, 563, 1352, 160);
		contentPane.add(tableItemsData);
		
		JLabel lblOrderSummary = new JLabel("Order Summary");
		lblOrderSummary.setFont(new Font("Tahoma", Font.BOLD, 30));
		lblOrderSummary.setBackground(Color.RED);
		lblOrderSummary.setBounds(50, 34, 339, 37);
		contentPane.add(lblOrderSummary);
		
		//Updated @Albert
		btnBackPage2 = new JButton("Back");
		btnBackPage2.setBounds(99, 851, 115, 29);
		contentPane.add(btnBackPage2);
	}
}